package com.example.egen.ecommerce.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;



@Entity
@Table(name = "orders")
public class Order {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
	
	
	@Column(name="order_id",unique=true)
	public String orderId;
	
	
	public Order() {}
	
	
	
   
    
	 @OneToMany(mappedBy = "orderObjInPayment", fetch = FetchType.LAZY,
	            cascade = CascadeType.ALL)
	public List<Payment> payments = new ArrayList<>();
    
    
    
    
    
    
    
    
  
    
   
    
    
    public void setOrderId(String orderId) {
		this.orderId = orderId;
    }
    
    
    public String getOrderId() {
		return orderId;
    	
    }
    	
    
    public Order( String orderId) {
       
                this.orderId=orderId;
        
    }
    
    
//other setters and getters
    
  
	
}
