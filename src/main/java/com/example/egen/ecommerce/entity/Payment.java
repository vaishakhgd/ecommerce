package com.example.egen.ecommerce.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "payments")
public class Payment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="payment_id",unique=true)
	public int payment_id;
	
	@Column(name="payment_type")
	public String payment_type;
	
	
	public Payment() {}
	
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "order_id")
	public Order orderObjInPayment;
	
	
	 public Payment(  String paymentType) {
	       
	        
	        this.payment_type = paymentType;
	       
	    }
	 
	 
	 
	    
	    
	    
	    public void setPaymentType(String paymentType) {
			this.payment_type = paymentType;
	    }
	    
	    
	    public String getPaymentType() {
			return payment_type;
	    	
	    }
	    
	    
	//other setters and getters
	    
	   
	
	

}
