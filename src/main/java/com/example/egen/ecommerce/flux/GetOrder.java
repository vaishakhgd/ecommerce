package com.example.egen.ecommerce.flux;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.egen.ecommerce.entity.Order;
import com.example.egen.ecommerce.entity.Payment;
import com.example.egen.ecommerce.repository.OrderRepository;
import com.example.egen.ecommerce.repository.PaymentRepository;

import reactor.core.publisher.Mono;

@Component
public class GetOrder {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	public Mono<Order> saveOrder(Order order) {
		
		CompletableFuture <Order> future= CompletableFuture
				.supplyAsync(() -> {
					
					Order savedOrder=orderRepository.save(order);
					
					return savedOrder;
				});
		Mono<Order> monoOrderFuture = Mono.fromFuture(future);
		return monoOrderFuture;
		
	}
	
	
public Mono<Payment> savePayment(Payment payment) {
		
		CompletableFuture <Payment> future= CompletableFuture
				.supplyAsync(() -> {
					
					Payment savedPayment=paymentRepository.save(payment);
					
					return savedPayment;
				});
		Mono<Payment> monoOrderFuture = Mono.fromFuture(future);
		return monoOrderFuture;
		
	}
	

}
