package com.example.egen.ecommerce.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.egen.ecommerce.dto.RequestDto;
import com.example.egen.ecommerce.entity.Order;
import com.example.egen.ecommerce.entity.Payment;
import com.example.egen.ecommerce.flux.GetOrder;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RestController
@RequestMapping("/fluxorder")
public class FluxOrderController {
	
	@Autowired
	public GetOrder getOrder;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping("/submit")
	public Mono saveOrderAsync(@RequestBody RequestDto requestDto){
		
		Order order=modelMapper.map(requestDto, Order.class);
    	order.orderId = requestDto.orderId;
    	Payment payment=modelMapper.map(requestDto.payment, Payment.class);
    	payment.payment_type = requestDto.payment.payment_type;
    	payment.orderObjInPayment =order;
    	order.payments.add(payment);
    	
    	Mono<Order> savedOrder= getOrder
    			                .saveOrder(order)
    			                .subscribeOn(Schedulers.elastic());
    	
    	
    	Mono<Payment> savedPayment = getOrder
    			.savePayment(payment)
    			.subscribeOn(Schedulers.elastic());
    	
    	Mono obj= Mono.zip(savedOrder,savedPayment);
		
    	return obj;
		
	}

}
