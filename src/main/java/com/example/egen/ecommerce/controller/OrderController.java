package com.example.egen.ecommerce.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.egen.ecommerce.dto.RequestDto;
import com.example.egen.ecommerce.dto.mapper.MapStructMapper;
import com.example.egen.ecommerce.entity.Order;
import com.example.egen.ecommerce.entity.Payment;
import com.example.egen.ecommerce.repository.OrderRepository;
import com.example.egen.ecommerce.repository.PaymentRepository;


@RestController
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	
	
	@Autowired
	private ModelMapper modelMapper;
	
	
	@GetMapping("/get/{id}")
    public ResponseEntity<Order> get(@PathVariable(value= "id") String order_id) {
		
		Order order = orderRepository.findOrderByOrderId(order_id);
		//return order.toString();
		return new  ResponseEntity<Order>(order,HttpStatus.OK) ;
		
	}
	
	

    
    @PostMapping("/submit")
    public ResponseEntity<Order> add(@RequestBody RequestDto requestDto) {
    	//System.out.println(order);
       // userService.saveUser(user);
    	//User user2= userRepository.save(user);
    	
    	//return orderRepository.save(order);
    	//return user2.toString();
    	//PostDto postDto = modelMapper.map(post, PostDto.class);
    	
    	
    	Order order=modelMapper.map(requestDto, Order.class);
    	order.orderId = requestDto.orderId;
    	Payment payment=modelMapper.map(requestDto.payment, Payment.class);
    	payment.payment_type = requestDto.payment.payment_type;
    	payment.orderObjInPayment =order;
    	order.payments.add(payment);
    	
    	
    	
    	//Payment payment=modelMapper.map(requestDto.payment, Payment.class);
    	
    	
    	
        
       Order savedEntity= orderRepository.save(order);
        paymentRepository.save(payment);
        
        return new ResponseEntity<Order>(savedEntity,HttpStatus.OK);
    	
        
     // return  order.toString() + "$" + payment.toString();
        
    }
    
   
    
   
}
