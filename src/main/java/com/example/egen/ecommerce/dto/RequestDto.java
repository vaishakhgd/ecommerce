package com.example.egen.ecommerce.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


import java.util.Date;


@Getter
@Setter
public class RequestDto {
	
	@JsonProperty("orderId")
    public String orderId;
	
	@JsonProperty("payment")
	public PaymentDto payment;

}
