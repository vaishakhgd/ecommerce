package com.example.egen.ecommerce.dto.mapper;

import com.example.egen.ecommerce.dto.PaymentDto;
import com.example.egen.ecommerce.dto.RequestDto;
import com.example.egen.ecommerce.entity.Order;
import com.example.egen.ecommerce.entity.Payment;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper(
        componentModel = "spring"
)

public interface MapStructMapper {

	MapStructMapper INSTANCE = Mappers.getMapper( MapStructMapper.class );
	
    Order  requestDtoToOrder(
            RequestDto requestDto
    );
    
    Payment paymentDtoToPayment(PaymentDto paymentDto);
}