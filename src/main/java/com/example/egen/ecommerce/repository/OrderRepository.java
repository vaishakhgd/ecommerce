package com.example.egen.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.egen.ecommerce.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
	
	@Query(value="select * from orders where order_id=:order_id LIMIT 1",nativeQuery=true)
	Order findOrderByOrderId(@Param("order_id")  String order_id);
}